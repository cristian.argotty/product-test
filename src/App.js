import React, {Component} from 'react';
import logo from './logo.svg';
import ProductDetail from './components/Product/ProductDetail';
import PRICE from './components/Product/ProductDetail/constans';
import './App.css';

const data = {
    productTitle: "Zapatillas",
    price: 10000,
    size: 41,
    priceT: PRICE
};

class App extends Component {

    constructor() {
        super();
        this.state = {
            data: data,
            category: 'Deportes'
        };
    };

    handleClick = () => {
        this.setState({
            category: 'Masculino'
        });
    };

    render() {
        return (
            <div className="App">
                <h1>{this.state.category}</h1>
                <header className="App-header">
                    <ProductDetail data={data}/>
                    <img src={logo} className="App-logo" alt="logo"/>
                    <button onClick={this.handleClick}>actualizar</button>
                </header>
            </div>);
    }
}

export default App;
