import React from 'react';
import { FaSurprise } from 'react-icons/fa';
import PropTypes from 'prop-types';
import './style.css';

const ProductDetail = ({data: {productTitle,size, price,priceT}}) => {

    const formatPrice = `${priceT} -> ${price} $`;

    return (
        <div className="detailCont">
            <h1><FaSurprise />{productTitle}</h1>
            <span>{formatPrice}</span>
        </div>
    )

};

ProductDetail.propTypes= {
    data: PropTypes.shape({
        productTitle: PropTypes.string.isRequired,
        size: PropTypes.number.isRequired,
        price: PropTypes.number.isRequired,
        priceT: PropTypes.any.isRequired
    }),

};
export default ProductDetail;



